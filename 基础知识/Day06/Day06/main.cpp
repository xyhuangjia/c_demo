//
//  main.cpp
//  Day06
//
//  Created by huangj on 15/1/18.
//  Copyright © 2018年 黄 佳. All rights reserved.
//

#include <iostream>
using namespace std;




const double Pi =3.14159;
double Area(double inputRadius);
double Circumference(double inputRadius);

double surfaceArea(double radius , double height);

// 表面积计算
void makeSurface (){
    cout << "enter a the radius of cylinder: ";
    double inradius = 0;
    cin >> inradius;
    cout  << "enter the height of cylinder :";
    double inheight = 0;
    cin >> inheight;
    cout << "surface area: "<< surfaceArea(inradius, inheight)<< endl;
}

//MARK:  斐波纳切函数
int getFibnumber(int FibIndex);

void runFibNumber(){
    cout << "输入数据：";
    int index = 0;
    cin >> index;
    cout << "fib numbers is "<< getFibnumber(index) << endl;
}

int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
    
    
//    cout << " enter  radius :" << endl;
//    double radius = 0;
//    cin >> radius;
//    cout << "area is :" << Area(radius) << endl;
//    cout << "circumference： " << Circumference(radius) << endl;
    
    
    
    //表面积计算
//    makeSurface();
    //斐波纳切函数
    runFibNumber();
    
    
    return 0;
}

double Area(double inputRadius){
    return Pi * inputRadius * inputRadius;
}

double Circumference(double inputRadius){
    return 2 * Pi * inputRadius;
}
//MARK:  表面积计算
double surfaceArea(double radius , double height){
    return 2 * Pi * radius * radius + 2*Pi *radius* height;
}
int getFibnumber(int FibIndex){
    if (FibIndex <  2) {
        return FibIndex;
    }else{
        return getFibnumber(FibIndex -1) + getFibnumber(FibIndex -2);
    }
}
