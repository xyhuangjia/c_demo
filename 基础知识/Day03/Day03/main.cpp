//
//  main.cpp
//  Day03
//
//  Created by huangj on 11/1/18.
//  Copyright © 2018年 黄 佳. All rights reserved.
//

#include <iostream>
#include <vector>
#include <string>
using namespace std;

enum CardinalDirection{
    north,
    south,
    east,
    west,
};

void showDirection(){
    // insert code here...
    cout << "展示方位" << endl;
    cout << "东方" << east <<endl;
    cout << "西方" << west <<endl;
    cout << "北方" << north <<endl;
    cout << "南方" << south <<endl;
};


void mutableArray(){
    vector<int> DynArrrNums (3);
    DynArrrNums[0] = 365;
    DynArrrNums[1] = -421;
    DynArrrNums[2] = 789;
    cout << "number of  integers in array:" << DynArrrNums.size() <<endl;
    cout << "enter another number for the array" << endl;
    int anotherNum = 0;
    cin >> anotherNum;
    //往数组掺入数据
    DynArrrNums.push_back(anotherNum);
    cout << "num of  intergers in array:"  << DynArrrNums.size()  << endl;
    cout << "last element in array:";
    cout << DynArrrNums[DynArrrNums.size() - 1]  << endl;
}



void cinString(){
    string greeting ("hello string!");
    cout << greeting << endl;
    
    cout << " enter a line of text " << endl;
    string fristline;
    getline(cin, fristline);
    
    cout << " enter a anotherline of text " << endl;
    string secondline;
    getline(cin, secondline);
    
    cout << " reault of text " << endl;
    string threeline = fristline + " " + secondline;
    cout << threeline <<  endl;
    
    cout << " copy of reault " << endl;
    string Copy;
    Copy = threeline;
    cout << Copy <<  endl;
    
    cout << " lenght of reault :" << threeline.length() << endl;
    
    
};


int main(int argc, const char * argv[]) {
//    showDirection();
//    mutableArray();
    cinString();
    return 0;
}
