//
//  main.cpp
//  单目递减运算符
//
//  Created by huangj on 2018/5/2.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;

class Date {
private:
    int Day;
    int Month;
    int Year;
    
public:
    Date (int InputDay,int InputMonth,int InputYear):
    Day(InputDay), Month(InputMonth),Year(InputYear){};
    Date& operator ++(){
        ++ Day;
        return *this;
    }
    Date& operator --(){
        --Day;
        return *this;
    }
    void DisplayDate(){
        cout << Day << "/" << Month << "/" << Year << endl;
    }
};

int main(int argc, const char * argv[]) {

    Date Holiday(25,12,2015);
    cout << "The Date object is initialized to: ";
    Holiday.DisplayDate();
    ++ Holiday;
    cout << "Date after prefix-increment is: ";
    Holiday.DisplayDate();
    -- Holiday;
    -- Holiday;
    cout << "Date after two prefix-decrments is :";
    Holiday.DisplayDate();
    
    return 0;
}
