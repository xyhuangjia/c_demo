//
//  main.cpp
//  派生类构造函数
//
//  Created by huangj on 2018/4/20.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;

class Fish {
private:
    bool FreshWaterFish;
    
public:
    Fish(bool IsFreshWater) : FreshWaterFish(IsFreshWater){}
    void Swim(){
        if (FreshWaterFish) {
            cout << "Swims is in lake" << endl;
        } else {
            cout << "Swims is in sea" << endl;
        }
    }
};

class Tuna: public Fish{
public:
    //金枪鱼
    Tuna():Fish(false){}
    void Swim(){
        cout << "Tuna swims real fast " << endl;
    }
};

class Carp: public Fish{
public:
    //鲤鱼
    Carp():Fish(true){}
    //覆盖方法
    void Swim(){
        cout << "Carp swims real slow " << endl;
        Fish::Swim();
    }
};


int main(int argc, const char * argv[]) {
    Carp myLunch;
    Tuna myDinner;
    cout << "getting my food to swim" << endl;
    cout << "lanch :" ;
    myLunch.Swim();
    cout << "Dinner :";
    myDinner.Swim();
    cout << "调用父类定义的方法：";
    myDinner.Fish::Swim();
    return 0;
}
