//
//  main.cpp
//  私有继承
//
//  Created by huangj on 2018/4/22.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>
/**
 公有继承，public
 私有继承，private
 保护继承 protectd
 */
using namespace std;
class Moter{
    
    
public:
    void SwitchIgnition(){
        cout << "Ignition ON"<< endl;
    }
    void PumpFuel(){
        cout << "Fuel in cylinders" <<endl;
    }
    void FireCylinder(){
        cout << "Vrooom" << endl;
    }
};
////class Car:private Moter {
class Car:protected Moter {
 /**
  *他表示Has-a的表示关系；
  *它让派生类能够访问基类的公有成员。
  *在继承层次的结构外面，也不能通过派生类实例访问基类的公有成员
  */

public:
    void Move(){
        SwitchIgnition();
        PumpFuel();
        FireCylinder();
    }
};

//class Car {
//private:
//    Moter heartOfCar;
//
//public:
//    void Move(){
//        heartOfCar.SwitchIgnition();
//        heartOfCar.PumpFuel();
//        heartOfCar.FireCylinder();
//    }
//};

class SuperCar:protected Car {
    
    
public:
    void Move(){
        SwitchIgnition();
        PumpFuel();
        FireCylinder();
        FireCylinder();
        FireCylinder();
    }
};

int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
//    Car myDreamCar;
//    myDreamCar.Move();
    SuperCar myCar;
    myCar.Move();
    return 0;
}
