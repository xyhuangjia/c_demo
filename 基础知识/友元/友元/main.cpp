//
//  main.cpp
//  友元
//
//  Created by huangj on 2018/4/19.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>
#include <string>

using namespace std;
class Humann {
private:
    string Name;
    int Age;
    /**
     友元：通过让函数成为类的友元，可以为赋予该函数与类的成员函数相同的访问权限。
     */
//    friend void DisplayAge(const Humann&Person);
    friend class Utility;
public:
    Humann(string inputName,int inputAge){
        Name = inputName;
        Age = inputAge;
    }
};
class Utility {
    
    
public:
    static void DisplayAge(const Humann& Person){
        cout<< Person.Age<< endl;
    }
};
//void DisplayAge(const Humann&Person){
//    cout << Person.Age <<endl;
//}
int main(int argc, const char * argv[]) {
    Humann FristMan("admin", 25);
    cout << "accessing private member age via friend :";
//    DisplayAge(FristMan);
    Utility::DisplayAge(FristMan);
    return 0;
}
