#include <iostream>
#include <string>

using namespace std;

class Human{
	private: string Name;
			 int Age;
	public:
		Human(){
			Age =0;
			cout << "测试信息" << endl;
		}
		
		Human(string HumanName){
			Name = HumanName;
			Age = 0;
			cout << " overload name is " << Name << endl;
		}
		
		Human(string HumanName ,int HumanAge){
			Name = HumanName;
			Age = HumanAge;
			cout<< " name is" << Name << "of"<< Age << "year old" << endl;
		}
		void setName(string HumanName){
			Name = HumanName;
		}
		void setAge(int HumanAge){
			Age = HumanAge;
		}
		void introduceSelf(){
			cout << "I'm" << Name << " and 'm  " ;
			cout << Age << " year old" << endl;
		}
};

int main(int argc, char *argv[]) {
	
	Human firstMan;
	firstMan.setName("tom");
	firstMan.setAge(25);
	
	Human secondMan ("jerry");
	secondMan.setAge(40);
	
	firstMan.introduceSelf();
	secondMan.introduceSelf();

}