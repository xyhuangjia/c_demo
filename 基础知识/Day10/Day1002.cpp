#include <iostream>
#include <string>

using namespace std;

class Human
{
private:
	string Name;
	int Age;

public:
	
	Human(){
		Age = 0;
		cout << "+++++++++++++"	<<endl;
	}
	
	void setName(string HumanName){
		Name = HumanName;
	}	
	void setAge(int HumanAge){
		Age = HumanAge;
	}
	
	int GetAge(){
		if (Age > 30) {
			return (Age - 2);
		}else {
			return Age;
		}
	}
	
	void introduceSelf(){
		cout << " im "<< Name << " and ";
		cout << " age is " << Age << " years old "<<endl; 
	}
};

int main(int argc, char *argv[]) {
	
	Human firstMan;
	firstMan.setName("tom");
	firstMan.setAge(25);
	
	Human secondMan;
	secondMan.setName("jerry");
	secondMan.setAge(40);
	
	firstMan.introduceSelf();
	secondMan.introduceSelf();
	
 	cout << "age is" << 	firstMan.GetAge() << endl;
	cout << "age is" << 	secondMan.GetAge() << endl;
	

}