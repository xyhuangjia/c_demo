//
//  main.cpp
//  智能指针
//
//  Created by huangj on 2018/5/20.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>
#include <memory.h>

using namespace std;

class Date {
private:
    int Day;
    int Month;
    int Year;
    string DateInString;
    
public:
    /*
     智能指针作用域，超出作用域自动释放指针，重新赋值也是会释放指针
     */
    Date(int InputDay,int InputMonth,int InputYear):Day(InputDay), Month(InputMonth) ,Year(InputYear) {
        
    };
    void DisPlayDate(){
        cout <<Day << "/" << Month << "/" << Year << endl;
    }
};

int main(int argc, const char * argv[]) {

    unique_ptr<int> pDynamicAllocInterger(new int);
    *pDynamicAllocInterger = 42;
    cout << "integer vlaue is " << *pDynamicAllocInterger << endl;
    
    unique_ptr<Date> pHoliday (new Date(25,11,2011));
    cout << "The new instance of date contains: " << endl;
    pHoliday->DisPlayDate();
    
   
    return 0;
}
