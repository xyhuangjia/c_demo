//
//  main.cpp
//  加法双目运算符
//
//  Created by huangj on 2018/5/22.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;

class Date {
private:
    int Day,Month,Year;
    
public:
    Date(int InputDay, int InputMonth,int InputYear):Day(InputDay),Month(InputMonth),Year(InputYear){};
    
    /**
     加法操作

     @param DaysToAdd <#DaysToAdd description#>
     @return <#return value description#>
     */
    Date operator + (int DaysToAdd){
        Date newDate (Day + DaysToAdd,Month,Year);
        return newDate;
    }
    /**
     法法操作
     
     @param DaysToSub <#DaysToSub description#>
     @return <#return value description#>
     */
    Date operator - (int DaysToSub){
        Date newDate (Day + DaysToSub,Month,Year);
        return newDate;
    }
    
    
    /**
     <#Description#>

     @param DaysToAdd <#DaysToAdd description#>
     @return <#return value description#>
     */
    void operator += (int DaysToAdd){
        Day += DaysToAdd;
    }

    /**
     <#Description#>

     @param DaysToAdd <#DaysToAdd description#>
     @return <#return value description#>
     */
    void operator -= (int DaysToAdd){
        Day -= DaysToAdd;
    }
    /**
     输出
     */
    void DisPlayDate(){
        cout << Day << " / "<< Month << "/" << Year << endl;
    }
};

int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
    Date Holiday(25,12,2011);
    cout << "Holiday on:";
    Holiday.DisPlayDate();
    
    Date preHoliday (Holiday - 19);
    cout << "preHoliday on:";
    preHoliday.DisPlayDate();
    
    Date nextHoliday(Holiday + 6);
    cout << "nextHoliday on:";
    nextHoliday.DisPlayDate();
    
    return 0;
}
