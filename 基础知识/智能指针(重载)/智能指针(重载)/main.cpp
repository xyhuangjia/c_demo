//
//  main.cpp
//  智能指针(重载)
//
//  Created by huangj on 2018/5/20.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;

template <typename T>
class smart_pointer {
private:
    T * m_pRawPointer;
    
public:
    smart_pointer(T* pData):m_pRawPointer(pData){};
    smart_pointer(){
        delete m_pRawPointer;
    }
    T& operator*() const{
        return *(m_pRawPointer);
    }
    T* operator-> () const{
        return m_pRawPointer; 
    }
};

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";
    return 0;
}
