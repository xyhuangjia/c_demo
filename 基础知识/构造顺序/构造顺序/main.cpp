//
//  main.cpp
//  构造顺序
//
//  Created by huangj on 2018/4/20.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;

class FishDummyMember {
    
public:
    FishDummyMember(){
      cout << "FishDummyMember constructor" << endl;
    }
    ~FishDummyMember(){
      cout << "FishDummyMember destructor" << endl;
    }
};

class Fish {
protected:
    FishDummyMember dummy;
    
public:
    Fish(){
        cout << "Fish constructer" << endl;
    }
    ~Fish(){
        cout << "Fish destructor" << endl;
    }
};
class TunaDummyMember {
    
    
public:
    TunaDummyMember(){
        cout << "TunaDummyMember constructer" << endl;
    }
    ~TunaDummyMember(){
        cout << "TunaDummyMember destructor" << endl;
    }
};

class Tuna: public Fish {
private:TunaDummyMember dummy;
    
public:
    Tuna(){
        cout << "Tuna constructor" << endl;
    }
    ~Tuna(){
        cout << "Tuna destructor" << endl;
    }
};
int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
    Tuna mydinner;
    return 0;
}
