//
//  main.cpp
//  多继承
//
//  Created by huangj on 2018/4/22.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;

/**
 *多继承：多继承即一个子类可以有多个父类，它继承了多个父类的特性。
 */
class Mammal {
    
    
public:
    void FeedBabyMilk(){
        cout << "Mammal:Baby says glug!" << endl;
    }
};

class Reptile {
    
    
public:
    void spitVenom(){
        cout << "Reptie:Shoo enemy! Spits venom!" <<endl;
    }
};

class Brid {
    
    
public:
    void layEggs(){
        cout << "Brid: laid my eggs ,am lighter now!" <<endl;
    }
};

class Platypus:public Mammal,public Brid,public Reptile {
    
    
public:
    void Swim(){
        cout << "我是鸭嘴兽" << endl;
    }
};
/*
 is a  和 has a之间的关系
 */
int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
    Platypus realFreak;
    realFreak.layEggs();
    realFreak.FeedBabyMilk();
    realFreak.spitVenom();
    realFreak.Swim();
    return 0;
}
