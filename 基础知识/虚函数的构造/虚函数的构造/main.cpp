//
//  main.cpp
//  虚函数的构造
//
//  Created by huangj on 2018/5/1.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;
class Fish {
    
public:
    Fish(){
        cout << "Constructed Fish" << endl;
    }
   virtual ~Fish(){
        cout << "Destroyed Fish" << endl;
    }
};

class Tuna:public Fish {
    
public:
    Tuna(){
        cout << "Constructed Tuna" << endl;
    }
    ~Tuna(){
        cout << "Destroyed Tuna" << endl;
    }
};

void DeleteFishMemory(Fish* pFish){
    delete pFish;
}

int main(int argc, const char * argv[]) {

    cout << "Allocating a Tuna on the free store:" << endl;
    Tuna *pTuna = new Tuna;
    cout << "Deleting the Tuna: " << endl;
    DeleteFishMemory(pTuna);
    cout << "Instantiating a Tuna on the stack:" << endl;
    Tuna myDinner;
    cout << "Automatic destruction as it goes of scope:" << endl;
    return 0;
}
