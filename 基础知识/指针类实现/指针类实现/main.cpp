//
//  main.cpp
//  指针类实现
//
//  Created by huangj on 2018/5/6.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;
template <typename T>

class smart_pointer {
private:
    T* m_RawPointer;
public:
    smart_pointer(T *pDate) : m_RawPointer(pDate){};
    ~smart_pointer(){
        delete m_RawPointer;
    };
    T& operator * () const {
        return *(m_RawPointer);
    }
    T *operator -> ()const{
        return m_RawPointer;
    };
};

class Date {
private:
    int Day,Month,Year;
    string DateInString;

public:
    Date (int InputDay,int InputMonth, int InputYear):
    Day(InputDay),Month(InputMonth),Year(InputYear){};
    void DisplayDate(){
        cout <<Day << "/" << Month << "/"<< Year << endl;
    }
};

int main(int argc, const char * argv[]) {
    smart_pointer <int> pDynamicInt (new int (42));
    cout << "Dynamically allocated integer value = " << *pDynamicInt << endl;
    smart_pointer <Date> pDate(new Date(25,12,2018));
    cout << "Date is = ";
    pDate->DisplayDate();
    
    return 0;
}
