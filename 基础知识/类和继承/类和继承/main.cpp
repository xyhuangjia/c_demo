//
//  main.cpp
//  类和继承
//
//  Created by huangj on 2018/4/20.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;
class Fish {
    
public:
    bool FreshWaterFish;
    void Swim(){
        if (FreshWaterFish) {
            cout << "swims in lake" << endl;
        } else {
            cout << "swims in sea" << endl;
        }
    }
};

class Tuna: public Fish {
    
public:
    Tuna(){
        FreshWaterFish = false;
    }
};

class Carp: public Fish {
    
public:
    Carp(){
        FreshWaterFish = true;
    }
};

int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
    Carp myLunch;
    Tuna myDinner;
    cout << "getting my food to swim" << endl;
    cout << "lanch :" ;
    myLunch.Swim();
    cout << "Dinner :";
    myDinner.Swim();
    return 0;
}
