//
//  main.cpp
//  day09
//
//  Created by huangj on 18/1/18.
//  Copyright © 2018年 黄 佳. All rights reserved.
//

#include <iostream>

using namespace std;

void  calcArea(const double * const pPi,
               const double *const pRadius,
               double * const pArea)
{
    if (pPi && pArea && pArea) {
        *pArea = (*pPi) * (*pRadius) * (*pRadius);
    }
}

int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
    const double pi = 3.1416;
    cout << "enter radius of cricle:";
    double radius = 0;
    cin >> radius;
    double area = 0;
    calcArea(&pi, &radius, &area);
    cout << " area is："<< area << endl;
    return 0;
}
