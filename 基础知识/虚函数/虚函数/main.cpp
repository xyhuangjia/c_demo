//
//  main.cpp
//  虚函数
//
//  Created by huangj on 2018/4/30.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;

class Fish {
    
public:
    virtual void Swim(){
        cout << "Fish Swims" << endl;
    }
};

class Tuna:public Fish {
    
public:
    void Swim(){
        cout << "Tuna Swims" << endl;
    }
};

class Carp:public Fish {
    
public:
    void Swim(){
        cout << "Carp Swims" << endl;
    }
};

void MakeFishSwim(Fish& inputFish){
    inputFish.Swim();
}

int main(int argc, const char * argv[]) {
   
    Tuna myDinner;
    Carp myLunch;
//
    MakeFishSwim(myDinner);
//
    MakeFishSwim(myLunch);
    return 0;
}
