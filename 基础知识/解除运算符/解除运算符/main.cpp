//
//  main.cpp
//  解除运算符
//
//  Created by huangj on 2018/5/4.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>
#include <memory.h>

using namespace std;

class Date {
private: int Day;
        int Month ;
        int Year;
        string DateToString;
    
public:
    Date(int InputDay,int InputMonth ,int InputYear):
    Day(InputDay),Month(InputMonth),Year(InputYear){};
    void DisplayDate(){
        cout << Day << "/"<< Month <<"/"<< Year << endl;
    }
};
int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
    unique_ptr<int> pDaynamicAllocInteger (new int);
    *pDaynamicAllocInteger = 42;
    cout << "Integer value is : "<< *pDaynamicAllocInteger  << endl;
    unique_ptr<Date> pHoliday(new Date(25,11,2018));
    cout << "the new isntance of date contains: " << endl;
    pHoliday->DisplayDate();
    return 0;
}
