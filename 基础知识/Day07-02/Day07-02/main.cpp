//
//  main.cpp
//  Day07-02
//
//  Created by huangj on 2018/1/16.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>
#include <string>

using namespace std;
//MARK: 指针定位
void setPoint(){
    int age = 10;
    int dogage = 9;
    cout << "age is :" << age << endl;
    cout << "dogage is" << dogage << endl;
    int * pinteger = &age;
    cout << "pinteger points to age:" << endl;
    cout << "pinteger = 0x " << hex << pinteger<< endl;
    cout << "*pinter =" << dec << * pinteger << endl;
    pinteger = &dogage;
    cout << " now "<<endl;
    cout << "pinteger = 0x " << hex << pinteger<< endl;
    cout << "*pinter = " << dec << * pinteger << endl;
    
}
//MARK: new and delete

void newandDeeleteoraption(){
    cout << " enter your name :";
    string name;
    cin >> name;
    int charstoalloctae = name.length() + 1;
    char * copyname = new char[charstoalloctae];
    strcpy(copyname, name.c_str());
    cout << "动态开创 字节包含：" << copyname << endl;
    delete [] copyname;
}

int main(int argc, const char * argv[]) {
    // insert code here...

    setPoint();
    newandDeeleteoraption();
    return 0;
}
