#include <iostream>

using namespace std;

class MyString{
	private:
		char *Buffer;
	public:
		MyString(const char* InitialInput){
			if (InitialInput != NULL) {
				Buffer = new char[strlen(InitialInput) +1];
				strcpy(Buffer, InitialInput);
			}else {
				Buffer = NULL;
			}
		}
		MyString(const MyString& CopySource){
			cout << "Copy construtor: copying from MyString" << endl;
			if (CopySource.Buffer != NULL) {
				Buffer = new char[strlen(CopySource.Buffer) + 1]; 
				strcpy(Buffer, CopySource.Buffer);
				cout << "Buffer points to :" << hex;
				cout << (unsigned int *)Buffer << endl;
			}else {
				Buffer = NULL;
			}
		}
		~MyString(){
			cout << "invoking destructor, clearing up" << endl;
			if (Buffer != NULL) {
				delete [] Buffer;
			}
		}
		int GetLength(){
			return strlen(Buffer);
		}
		const char * GetString(){
			return Buffer;
		}
};
void UseMyString(MyString Input){
	cout << "String buffer in MyString is " << Input.GetLength();
	cout << "Character long " << endl;
	cout << "Butter Contains: "<< Input.GetString() << endl;
	return;
}
int main(int argc, char *argv[]) {
	MyString SayHello("hello from string class");
	cout << "String buffer in MyString is " << SayHello.GetLength();
	cout << "characters long" << endl;
	cout << "Buffer contains:" ;
	cout << "Buffer contains: "<< SayHello.GetString()<< endl;
	UseMyString(SayHello);
}