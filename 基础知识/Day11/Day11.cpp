#include <iostream>
#include <string>

using namespace std;

class Human{
	private:
		string Name;
		int Age;
	public:
		Human(string HumansName,int HumanAge){
			Name = HumansName;
			Age = HumanAge;
			cout << "overload constructor creates " << Name;
			cout << " of age " << Age <<  endl;
		}
		void introduceSelf(){
			cout << "I'm " << Name << " and Age " << Age << " years old" << endl;
		}
			
};
int main(int argc, char *argv[]) {
	Human man = Human("aaa", 20);
	Human woman = Human("bbb", 30);
	man.introduceSelf();
	woman.introduceSelf();
}