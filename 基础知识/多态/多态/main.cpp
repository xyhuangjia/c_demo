//
//  main.cpp
//  多态
//
//  Created by huangj on 2018/4/22.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;

class Fish {
    
    
public:
    void Swim(){
        cout << "Fish swims!" << endl;
    }
};

class Tuna:public Fish {
    
    
public:
    void Swim(){
       cout << "Tuna swims!" << endl;
    }
};

void MakeFishSwim(Fish& inputFish){
    inputFish.Swim();
}
int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
    Tuna myDinner;
    myDinner.Swim();
    MakeFishSwim(myDinner);
    return 0;
}
