//
//  main.cpp
//  转换运算符
//
//  Created by huangj on 2018/5/3.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>
#include <sstream>
#include <string>

using namespace std;
class Date {
private:
    int Day;
    int Month;
    int Year;
    
    string DateInString;
public:
    Date(int InputDay,int InputMonth, int InputYear):
    Day(InputDay),Month(InputMonth), Year(InputYear){};
    operator const char*(){
        ostringstream formattedDate;
        formattedDate << Day << "/" << Month << "/" << Year << endl;
        DateInString = formattedDate.str();
        return DateInString.c_str();
    }
};
int main(int argc, const char * argv[]) {
    Date  Holiday (03, 05, 2018);
    cout << "Holiday:" << Holiday << endl;
    return 0;
}
