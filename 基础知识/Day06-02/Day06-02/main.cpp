//
//  main.cpp
//  Day06-02
//
//  Created by huangj on 15/1/18.
//  Copyright © 2018年 黄 佳. All rights reserved.
//

#include <iostream>


using namespace std;
const double Pi =3.14159;



//MARK:  重载
double area(double radius);
double area(double radius, double height);

void overLoad(){
    cout << "enter z for cylinder,c for circle";
    char choice='z';
    cin >> choice ;
    
    cout << "enter radius:";
    double radius =0;
    cin >> radius;
    
    if (choice == 'z') {
        cout << "enter height:";
        double height = 0;
        cin >> height;
        cout  <<  "area of  cylinder is : " << area(radius, height)<< endl;
    }else{
        cout  <<  "area of  cylinder is : " << area(radius)<< endl;
    }
}
//MARK: 内联函数
inline long DouleNum (int inputNum){
    return inputNum *2;
}
void runInline(){
    cout << "enter a interger:"<< endl ;
    int inputNum = 0;
    cin >> inputNum;
    cout << "double is："<< DouleNum(inputNum)<< endl;
    
}
int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
//    overLoad();
    runInline();
    return 0;
}
double area(double radius){
    return Pi * radius * radius ;
}
double area(double radius, double height){
    return 2*area(radius) + 2 *Pi *radius*height;
}
