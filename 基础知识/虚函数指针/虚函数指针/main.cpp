//
//  main.cpp
//  虚函数指针
//
//  Created by huangj on 2018/5/1.
//  Copyright © 2018年 huangj. All rights reserved.
//

#include <iostream>

using namespace std;
class SimpleClass {
    int a,b;
public:
    void FuncDoSomething(){
        
    }
};
class Base {
    int a,b;
    
public:
    virtual void FuncDoSomething(){}
};
int main(int argc, const char * argv[]) {
    // insert code here...
//    std::cout << "Hello, World!\n";
    cout << "sizeof(SimpleClass) = "<< sizeof(SimpleClass) << endl;
    cout << "sizeof(Base) = "<< sizeof(Base) << endl;
    return 0;
}
